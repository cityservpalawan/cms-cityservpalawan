<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCrTrackerHeaderAddedAppointmentSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cr_tracker_header', function($table)
        {
            $table->dateTime('appointment_schedule')->after('subcategory_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cr_tracker_header', function($table)
        {
            $table->dropColumn(array('appointment_schedule'));
        });
    }
}
