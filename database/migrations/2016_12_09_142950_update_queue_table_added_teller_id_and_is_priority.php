<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQueueTableAddedTellerIdAndIsPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('queue', function($table)
        {
            $table->integer('teller_id')->default(0)->after('id');
            $table->enum('is_priority',['yes','no'])->default('no')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('queue', function($table)
        {
            $table->dropColumn(array('teller_id','is_priority'));
        });
    }
}
