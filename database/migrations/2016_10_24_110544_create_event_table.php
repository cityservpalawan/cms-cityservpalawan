<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('user_id')->default(0);

            $table->string('title',150);
            $table->string('excerpt',255);

            $table->text('venue');
            $table->text('content');
            $table->text('slug');

            $table->text('directory')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('path')->nullable();

            $table->string('geo_lat',50)->nullable();
            $table->string('geo_long',50)->nullable();
            $table->dateTime('posted_at');
            $table->enum('status',["draft","published"])->default("draft");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event');
    }
}
