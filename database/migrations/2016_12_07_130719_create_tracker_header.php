<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackerHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cr_tracker_header', function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->integer('citizen_request_id')->default(0);
            $table->integer('cr_category_id')->default(0);
            $table->integer('cr_subcategory_id')->default(0);
            $table->string('category_title')->nullable();
            $table->string('category_code')->nullable();
            $table->string('subcategory_title')->nullable();
            $table->string('subcategory_code')->nullable();
            // $table->integer('cr_process_id')->default(0);
            // $table->integer('user_id')->default(0);
            // $table->string('process_title');
            // $table->string('process_code');
            // $table->dateTime('from');
            // $table->dateTime('to');
            // $table->integer('duration');
            // $table->text('remarks')->nullable();
            // $table->string('status',50)->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('cr_tracker_header');
    }
}
