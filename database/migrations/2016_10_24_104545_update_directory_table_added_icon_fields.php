<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDirectoryTableAddedIconFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('directory', function($table)
        {
            $table->text('directory')->nullable()->after('title');
            $table->string('filename',150)->nullable()->after('title');
            $table->text('path')->nullable()->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('directory', function($table)
        {
            $table->dropColumn(array('directory','filename','path'));
        });
    }
}
