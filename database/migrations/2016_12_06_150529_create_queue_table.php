<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('queue', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('service_type',50);
            $table->integer('sequence_no')->default(0);
            $table->string('queue_no',60);
            $table->string('status',50)->default('pending');
            $table->string('source')->default('webapp');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('queue');
    }
}
