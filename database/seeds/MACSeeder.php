<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\CRModule;
use App\Laravel\Models\CRCategory;
use App\Laravel\Models\CRSubCategory;
use App\Laravel\Models\CRProcess;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Str;

class MACSeeder extends Seeder{
	public function run(){

		CRModule::truncate();
	   	CRCategory::truncate();
		CRSubCategory::truncate();
		CRProcess::truncate();
		CRTracker::truncate();
		CRTrackerHeader::truncate();

		// Social Service
		$module = new CRModule;
		$module->fill(['title' => "Mayor's Action Center", 'code' => "mac"]);
		$module->save();

		$category = new CRCategory;
		$category->fill(['title' => "Social Service", 'code' => "social_service", 'cr_module_id' => $module->id]);
		$category->save();

		$subcategories = [
			['title' => "Medical Assistance", 'code' => "medical"],
			['title' => "Burial Assistance", 'code' => "burial"],
		];

		foreach ($subcategories as $subcategory) {
			$sub_category = new CRSubCategory;
			$sub_category->fill(['cr_category_id' => $category->id] + $subcategory);
			$sub_category->save();

			$processes = array();
			switch ($sub_category->code) {
				case 'medical':
				case 'burial':
					$processes = [
						['title' => "MAC Interview", 'code' => "mac_interview"],
						['title' => "Social Case Study", 'code' => "social_case_study"],
						['title' => "Certificate of Indigency, Making of Obligation Request and Disbursement", 'code' => "certificate_of_indigency_making_of_obligation_request_and_disbursement"],
						['title' => "CSWDO for signature", 'code' => "cswdo_for_signature"],
						['title' => "Budget", 'code' => "budget"],
						['title' => "Obligation Request Approval", 'code' => "obligation_request_approval"],
						['title' => "Accounting", 'code' => "accounting"],
						['title' => "Treasurer's Office for the cheque", 'code' => "treasurers_office_for_the_cheque"],
						['title' => "Mayor's Office for the signature", 'code' => "mayors_office_for_the_signature"],
						['title' => "Transmittal", 'code' => "transmittal"],
						['title' => "Cheque Release", 'code' => "cheque_release"],
					];
				break;
			}

			foreach ($processes as $index => $process) {
				CRProcess::create(['cr_category_id' => $category->id, 'cr_subcategory_id' => $sub_category->id, 'sequence' => ($index + 1)] + $process);
			}
		}
		// End of Social Service

		// Scholarship
		$category = new CRCategory;
		$category->fill(['title' => "Scholarship", 'code' => "scholarship", 'cr_module_id' => $module->id]);
		$category->save();

		$subcategories = [
			['title' => "Scholarship", 'code' => "scholarship"],
		];

		foreach ($subcategories as $subcategory) {
			$sub_category = new CRSubCategory;
			$sub_category->fill(['cr_category_id' => $category->id] + $subcategory);
			$sub_category->save();

			$processes = array();
			switch ($sub_category->code) {
				case 'scholarship':
					$processes = [
						['title' => "Initial Requirements", 'code' => "initial_requirements"],
						['title' => "Application Form", 'code' => "application_form"],
						['title' => "Interview and Discussion of Qualification", 'code' => "interview_and_discussion_of_qualification"],
						['title' => "Additional Requirements", 'code' => "additional_requirements"],
						['title' => "Filling-out of ATM account form", 'code' => "filling_out_of_atm_account_form"],
						['title' => "Disbursement Voucher and Obligation Request Preparation", 'code' => "disbursement_voucher_and_obligation_request_preparation"],
						['title' => "Checking of the availability of funds", 'code' => "checking_of_the_availability_of_funds"],
						['title' => "Auditing", 'code' => "auditing"],
						['title' => "Processing of Cheque", 'code' => "processing_of_cheque"],
						['title' => "Transmittal", 'code' => "transmittal"],
						['title' => "Release of Scholarship Grant", 'code' => "release_of_scholarship_grant"],
					];
				break;
			}

			foreach ($processes as $index => $process) {
				CRProcess::create(['cr_category_id' => $category->id, 'cr_subcategory_id' => $sub_category->id, 'sequence' => ($index + 1)] + $process);
			}
		}
		// End of Scholarship


	}
}