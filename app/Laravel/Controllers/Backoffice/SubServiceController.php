<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\SubService;
use App\Laravel\Models\Service;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\SubServiceRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class SubServiceController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
		$this->data['services'] = [ '' => "Choose service"] + Service::orderBy('title',"ASC")->lists('title','id')->toArray();
	}

	public function index () {
		$this->data['subservices'] = SubService::orderBy('updated_at',"DESC")->get();
		return view('backoffice.subservices.index',$this->data);
	}

	public function create () {
		return view('backoffice.subservices.create',$this->data);
	}

	public function store (SubServiceRequest $request) {
		try {
			$new_service = new SubService;
			$new_service->fill($request->all());
			$new_service->code = Helper::get_slug('sub_service','title',$request->get('title'));

			if($new_service->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A subservice has been added.");
				return redirect()->route('backoffice.subservices.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$service = SubService::find($id);

		if (!$service) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.subservices.index');
		}

		$this->data['subservice'] = $service;
		return view('backoffice.subservices.edit',$this->data);
	}

	public function update (SubServiceRequest $request, $id = NULL) {
		try {
			$service = SubService::find($id);

			if (!$service) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.subservices.index');
			}

			$service->fill($request->all());

			if($service->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A subservice has been updated.");
				return redirect()->route('backoffice.subservices.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$service = SubService::find($id);

			if (!$service) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.subservices.index');
			}

			if($service->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A subservice has been deleted.");
				return redirect()->route('backoffice.subservices.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}