<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ReportFooter;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ReportFooterRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class ReportFooterController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['footers'] = ReportFooter::orderBy('updated_at',"DESC")->get();
		return view('backoffice.report-footers.index',$this->data);
	}

	public function create () {
		return view('backoffice.report-footers.create',$this->data);
	}

	public function store (ReportFooterRequest $request) {
		try {
			$new_footer = new ReportFooter;
			$new_footer->fill($request->all());

			if($new_footer->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report footer has been added.");
				return redirect()->route('backoffice.report_footers.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$footer = ReportFooter::find($id);

		if (!$footer) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.report_footers.index');
		}

		$this->data['footer'] = $footer;
		return view('backoffice.report-footers.edit',$this->data);
	}

	public function update (ReportFooterRequest $request, $id = NULL) {
		try {
			$footer = Page::find($id);

			if (!$footer) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.report_footers.index');
			}

			$footer->fill($request->all());
			
			if($footer->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report footer has been updated.");
				return redirect()->route('backoffice.report_footers.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$footer = Page::find($id);

			if (!$footer) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.report_footers.index');
			}

			if($footer->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report footer has been deleted.");
				return redirect()->route('backoffice.report_footers.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}