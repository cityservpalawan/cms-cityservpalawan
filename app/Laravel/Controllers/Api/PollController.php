<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Poll;
use App\Laravel\Models\PollAnswer;

/**
*m
* Requests used for this controller
*/
use App\Laravel\Requests\Api\PollAnswerRequest;

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\PollTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use Helper, Carbon, Input, Str, ImageUploader;
use Request, GeoIp;

class PollController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$polls = Poll::where('status',"published")
					->orderBy('created_at',"DESC")
					->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($polls, new PollTransformer, 'collection');
			$this->response['has_morepage'] = $polls->hasMorePages();
			$this->response['msg'] = "Poll List.";
			$this->response['status_code'] = "POLL_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
			break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			$poll = Poll::where('id',Input::get('poll_id',0))
						->first();

			$this->response['data'] = $this->transformer->transform($poll, new PollTransformer, 'item');
			$this->response['msg'] = "Poll  Details.";
			$this->response['status_code'] = "POLL_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store(PollAnswerRequest $request, $format = "json"){
		try{	
			$new_poll = new PollAnswer;
			$new_poll->fill($request->all());

			$new_poll->user_id = $this->user_id;

			$poll_id = Input::get('poll_id');
			$choice = Input::get('choice');


			$new_poll->choice = $choice;
			$new_poll->poll_id = $poll_id;

			if($new_poll->save()) {
				$new_poll->save();
				$this->response['msg'] = "Your answer has been sent";
				$this->response['status_code'] = "ANSWER_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}