<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\Announcement;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\AnnouncementTransformer;

use Input, Str;


class AnnouncementController extends Controller{


	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function index($format = ""){
		try{
			$per_page = Input::get("per_page",10);
			$announcements = Announcement::where('status',"published")->orderBy('posted_at',"DESC")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($announcements,new AnnouncementTransformer,'collection');
			$this->response['has_morepage'] = $announcements->hasMorePages();
			$this->response['msg'] = "List of announcements.";
			$this->response['status_code'] = "ANNOUNCEMENT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function recent($format = ""){
		try{
			$announcements = Announcement::where('status',"published")->orderBy('posted_at',"DESC")->take(3)->get();

			$this->response['data'] = $this->transformer->transform($announcements,new AnnouncementTransformer,'collection');
			$this->response['msg'] = "Recent announcements.";
			$this->response['status_code'] = "RECENT_ANNOUNCEMENT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function featured($format = ""){
		try{
			$announcements = Announcement::where('status',"published")->where('featured',1)->orderBy('posted_at',"DESC")->take(3)->get();

			$this->response['data'] = $this->transformer->transform($announcements,new AnnouncementTransformer,'collection');
			$this->response['msg'] = "Featured announcements.";
			$this->response['status_code'] = "FEATURED_ANNOUNCEMENT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = ""){
		try{
			$announcement_id = Input::get('announcement_id');
			$announcement = Announcement::where('id',$announcement_id)->where('status',"published")->first();

			$this->response['data'] = $this->transformer->transform($announcement,new AnnouncementTransformer,'item');
			$this->response['msg'] = "Announcement Details.";
			$this->response['status_code'] = "ARTICLE_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}