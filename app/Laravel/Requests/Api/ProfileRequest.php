<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class ProfileRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		$rules = [
			'email'			=> 'required|email|unique_email:'.$this->get('auth_id'),
			'fname' 		=> "required",
			'lname'			=> "required",
			'contact_number'	=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'password_format'			=> "Invalid password format.",
			'unique_email'				=> "Email address already taken",
			'required'					=> "Field is required.",

		];
	}

}