<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class QueueRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'service_type' => 'required',
		];
	}

	public function messages(){
		return [
			'required' => "Field is required.",
		];
	}
}