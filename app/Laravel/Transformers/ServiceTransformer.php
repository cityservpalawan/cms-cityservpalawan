<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Service;

use App\Laravel\Transformers\SubServiceTransformer;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class ServiceTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','subservices','info'
    ];

	public function transform(Service $service){
	     return [
	     	'id' => $service->id,
	     	'title' => $service->title,
	     	'code' => $service->code,
	     ];
	}

	public function includeInfo(Service $service) {
		$collection = Collection::make([
			'content' => $service->content,
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeDate(Service $service){
        $collection = Collection::make([
			'date_db' => $service->date_db($service->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $service->month_year($service->created_at),
			'time_passed' => $service->time_passed($service->created_at),
			'timestamp' => $service->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeSubservices(Service $service){
		return $this->collection($service->subservices, new SubServiceTransformer);
	}
}