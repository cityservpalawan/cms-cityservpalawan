<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\SubService;
use App\Laravel\Models\Directory;

use App\Laravel\Transformers\ServiceTransformer;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

class SubServiceTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','service','info'
    ];

	public function transform(SubService $subservice)
	{
	    return [
	    	'id'			=> $subservice->id,
	    	'title'		=> $subservice->title,
	    	'code'		=> $subservice->code,
	    ];
	}

	public function includeInfo(SubService $service) {
		$collection = Collection::make([
			'content' => $service->content,
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeDate(SubService $subservice){
        $collection = Collection::make([
			'date_db' => $subservice->date_db($subservice->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $subservice->month_year($subservice->created_at),
			'time_passed' => $subservice->time_passed($subservice->created_at),
			'timestamp' => $subservice->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeService(SubService $subservice){
        $service = $subservice->service ? : new Service;
        if(!$service->id) $service->id = 0;
        return $this->item($service, new ServiceTransformer);
	}
	
}