	<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\MacRequest;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class MacRequestTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'more'
    ];

	public function transform(MacRequest $request){

	     return [
	     	'id' => $request->id,
	     	'schedule_appointment' => $request->schedule_appointment,
	     	'info_validation' => $request->info_validation_status,
			'assessment_evaluation' => $request->assessment_evaluation_status,
			'final_interview' => $request->final_interview_status,
			'requirement_submission' => $request->requirement_submission_status,
			'total_duration' => "",
	     ];
	}

	public function includeMore(MacRequest $request){
        $collection = Collection::make([
        	[
        		'title' => Str::title(str_replace('_', ' ', "info_validation")),
        		'code' => 'info_validation_status',
        		'duration' => ($request->info_validation_duration ? Helper::date_db($request->info_validation_to) : '?') ,
        		'status' => $request->info_validation_status 
        	],

			[
				'title' => Str::title(str_replace('_', ' ', "assessment_evaluation")),
				'code' => 'assessment_evaluation_status',
				'duration' => ($request->assessment_evaluation_duration ? Helper::date_db($request->assessment_evaluation_to) : '?') ,
				'status' => $request->assessment_evaluation_status 
			],

			[
				'title' => Str::title(str_replace('_', ' ', "final_interview")),
				'code' => 'final_interview_status',
				'duration' => ($request->final_interview_duration ? Helper::date_db($request->final_interview_to) : '?') ,
				'status' => $request->final_interview_status 
			],

			[
				'title' => Str::title(str_replace('_', ' ', "requirement_submission")),
				'code' => 'requirement_submission_status',
				'duration' => ($request->requirement_submission_duration ? Helper::date_db($request->requirement_submission_to) : '?') ,
				'status' => $request->requirement_submission_status 
			],
    	]);
        return $this->item($collection, new MasterTransformer);
	}
}