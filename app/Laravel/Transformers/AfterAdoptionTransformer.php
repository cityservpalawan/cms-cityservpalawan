<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\AfterAdoption;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class AfterAdoptionTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date','user'
    ];

	public function transform(AfterAdoption $request){
	     return [
	     	'id' => $request->id,
	     	'code' => $request->code,
	     	'lname' => $request->lname,
	     	'fname' => $request->fname,
	     	'middle_name' => $request->middle_name,
	     	'father_last_name'	=> $request->father_lname,
	     	'father_first_name' => $request->father_fname,
	     	'father_middle_name' => $request->father_middle_name,
	     	'father_occupation' => $request->father_occupation,
			'father_religion' => $request->father_religion,
			'father_age' => $request->father_age,
	     	'mother_last_name'	=> $request->mother_lname,
	     	'mother_first_name' => $request->mother_fname,
	     	'mother_middle_name' => $request->mother_middle_name,
	     	'mother_occupation' => $request->mother_occupation,
			'mother_religion' => $request->mother_religion,
			'mother_age' => $request->mother_age,
	     	// 'date_of_birth' => $request->date_of_birth,
	     	// 'place_of_birth' => $request->place_of_birth,
			'decree_issued' => $request->decree_issued,
			'final_decree' => $request->final_decree,
			'court_name' => $request->court_name,
			'judge_lname' => $request->judge_lname,
			'judge_fname' => $request->judge_fname,
			'judge_middle_name' => $request->judge_middle_name,

	     	'contact_number' => $request->contact,
	     	'purpose' => $request->purpose,
	     	'number_of_copies' => $request->number_of_copies,
	     	
	     	'requested_by' => $request->author ? : new User,
	     ];
	}

	public function includeDate(AfterAdoption $request){
        $collection = Collection::make([
			'date_db' => $request->date_db($request->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $request->month_year($request->created_at),
			'time_passed' => $request->time_passed($request->created_at),
			'timestamp' => $request->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(AfterAdoption $request){
		$collection = Collection::make([
			'sender'	=> $request->author ? "{$request->author->fname} {$request->author->lname}" : "Anonymous",
			'status'	=> $request->status,
			'code'	=> $request->code,
			'content' => $request->content,
			'geolocation'	=> [
    			"lat"			=> $request->geo_long,
    			"long"			=> $request->geo_lat,
    		],
 			'path' => $request->path,
 			'directory' => $request->directory,
 			'full_path' => $request->path ? "{$request->directory}/resized/{$request->filename}" : asset("{$request->directory}/resized/{$request->filename}"),
 			'thumb_path' => $request->path ? "{$request->directory}/thumbnails/{$request->filename}" : asset("{$request->directory}/thumbnails/{$request->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}

	public function includeUser(AfterAdoption $request){
       $user = $request->author ? : new User;
       if(is_null($user->id)){ $user->id = 0;}
       return $this->item($user, new UserTransformer);
    }
}