@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-certificate"></i> <span class="text-semibold">Mayor's Action Center</span> - Edit a MAC request.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.citizen_requests.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.citizen_requests.index')}}"> Mayor's Action Center</a></li>
			<li class="active"># {{ $request->tracking_number ? : "---" }}</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">MAC Request Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this MAC request.</p>.

				{{-- <div class="form-group {{$errors->first('module') ? 'has-error' : NULL}}">
					<label for="module" class="control-label col-lg-2 text-right">Module <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("module", $modules, old('module', $request->module), ['id' => "module", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('module'))
						<span class="help-block">{{$errors->first('module')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('category') ? 'has-error' : NULL}}">
					<label for="category" class="control-label col-lg-2 text-right">Category <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("category", $categories, old('category', $request->category), ['id' => "category", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('category'))
						<span class="help-block">{{$errors->first('category')}}</span>
						@endif
					</div>
				</div> --}}

				{{-- <div class="form-group {{$errors->first('subcategory') ? 'has-error' : NULL}}">
					<label for="subcategory" class="control-label col-lg-2 text-right">MAC Category <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("subcategory", $subcategories, old('subcategory', $request->subcategory), ['id' => "subcategory", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('subcategory'))
						<span class="help-block">{{$errors->first('subcategory')}}</span>
						@endif
					</div>
				</div> --}}

				<div class="form-group">
					<label for="task" class="control-label col-lg-2 text-right">MAC Category</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin">{{ $subcategory ? $subcategory->title : $request->subcategory }}</h6>
						<input type="hidden" name="subcategory" value="{{$request->subcategory}}">
					</div>
				</div>

				<div class="form-group">
					<label for="task" class="control-label col-lg-2 text-right">Request Code</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin">{{ $request->code }}</h6>
					</div>
				</div>

				<div class="form-group">
					<label for="task" class="control-label col-lg-2 text-right">Tracking #</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin">{{ $request->tracking_number }}</h6>
					</div>
				</div>

				@if($auth->type == "super_user")
				<div class="form-group {{$errors->first('user_id') ? 'has-error' : NULL}}">
					<label for="user_id" class="control-label col-lg-2 text-right">Citizen Account <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("user_id", $users, old('user_id',$request->user_id), ['id' => "user_id", 'class' => "select select-with-search col-xs-12 col-sm-12 col-md-12 col-lg-12"])!!}
						@if($errors->first('user_id'))
						<span class="help-block">{{$errors->first('user_id')}}</span>
						@endif
					</div>
				</div>
				@else
				<div class="form-group">
					<label for="task" class="control-label col-lg-2 text-right">Citizen Account</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin">{{ $request->author ? "{$request->author->fname} {$request->author->lname} ({$request->author->email}) - Brgy. {$request->author->barangay}": "---" }}</h6>
						<input type="hidden" name="user_id" value="{{$request->user_id}}">
					</div>
				</div>
				@endif

				{{-- <div class="form-group {{$errors->first('user_id') ? 'has-error' : NULL}}">
					<label for="user_id" class="control-label col-lg-2 text-right">User </label>
					<div class="col-lg-9">
						{!!Form::select("user_id", $users, old('user_id', $request->user_id), ['id' => "user_id", 'class' => "select select-with-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('user_id'))
						<span class="help-block">{{$errors->first('user_id')}}</span>
						@endif
					</div>
				</div> --}}

				<div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
					<label for="name" class="control-label col-lg-2 text-right">Client Name <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="name" id="name" placeholder="" maxlength="100" value="{{old('name', $request->name)}}">
						@if($errors->first('name'))
						<span class="help-block">{{$errors->first('name')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
					<label for="email" class="control-label col-lg-2 text-right">Client Email </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="email" id="email" placeholder="" maxlength="100" value="{{old('email', $request->email)}}">
						@if($errors->first('email'))
						<span class="help-block">{{$errors->first('email')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('school_id') ? 'has-error' : NULL}}">
					<label for="school_id" class="control-label col-lg-2 text-right">School / University </label>
					<div class="col-lg-9">
						{!!Form::select("school_id", $schools, old('school_id', $request->school_id), ['id' => "school_id", 'class' => "select select-with-search col-xs-12 col-sm-12 col-md-12 col-lg-12"])!!}
						<span class="help-block">If MAC category is Scholarship, please choose one from this list, otherwise leave this field blank.</span>
						@if($errors->first('school_id'))
						<span class="help-block">{{$errors->first('school_id')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('contact_number') ? 'has-error' : NULL}}">
					<label for="contact_number" class="control-label col-lg-2 text-right">Client Contact Number </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="contact_number" id="contact_number" placeholder="" maxlength="100" value="{{old('contact_number', $request->contact_number)}}">
						@if($errors->first('contact_number'))
						<span class="help-block">{{$errors->first('contact_number')}}</span>
						@endif
					</div>
				</div>


				<div class="form-group {{$errors->first('beneficiary_name') ? 'has-error' : NULL}}">
					<label for="beneficiary_name" class="control-label col-lg-2 text-right">Beneficiary Name </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_name" id="beneficiary_name" placeholder="" maxlength="100" value="{{old('beneficiary_name', $request->beneficiary_name)}}">
						@if($errors->first('beneficiary_name'))
						<span class="help-block">{{$errors->first('beneficiary_name')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_age') ? 'has-error' : NULL}}">
					<label for="beneficiary_age" class="control-label col-lg-2 text-right">Beneficiary Age </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_age" id="beneficiary_age" placeholder="" value="{{old('beneficiary_age', $request->beneficiary_age)}}">
						@if($errors->first('beneficiary_age'))
						<span class="help-block">{{$errors->first('beneficiary_age')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_birthdate') ? 'has-error' : NULL}}">
					<label for="beneficiary_birthdate" class="control-label col-lg-2 text-right">Beneficiary Birthdate </label>
					<div class="col-lg-9">
						<input type="text" id="beneficiary_birthdate" name="beneficiary_birthdate" class="dropup form-control daterange-single" placeholder="YYYY-MM-DD" value="{{old('beneficiary_birthdate', ($request->beneficiary_birthdate AND $request->beneficiary_birthdate != "0000-00-00")?$request->beneficiary_birthdate:NULL)}}">
						{{-- <input class="form-control" type="text" name="beneficiary_birthdate" id="beneficiary_birthdate" placeholder="" value="{{old('beneficiary_birthdate', $request->beneficiary_birthdate)}}"> --}}
						@if($errors->first('beneficiary_birthdate'))
						<span class="help-block">{{$errors->first('beneficiary_birthdate')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_birthplace') ? 'has-error' : NULL}}">
					<label for="beneficiary_birthplace" class="control-label col-lg-2 text-right">Beneficiary Birthplace </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_birthplace" id="beneficiary_birthplace" placeholder="" value="{{old('beneficiary_birthplace', $request->beneficiary_birthplace)}}">
						@if($errors->first('beneficiary_birthplace'))
						<span class="help-block">{{$errors->first('beneficiary_birthplace')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_gender') ? 'has-error' : NULL}}">
					<label for="beneficiary_gender" class="control-label col-lg-2 text-right">Beneficiary Gender </label>
					<div class="col-lg-9">
						{!!Form::select("beneficiary_gender", $genders, old('beneficiary_gender', $request->beneficiary_gender), ['id' => "beneficiary_gender", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('beneficiary_gender'))
						<span class="help-block">{{$errors->first('beneficiary_gender')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_civil_status') ? 'has-error' : NULL}}">
					<label for="beneficiary_civil_status" class="control-label col-lg-2 text-right">Beneficiary Civil Status </label>
					<div class="col-lg-9">
						{!!Form::select("beneficiary_civil_status", $civil_statuses, old('beneficiary_civil_status', $request->beneficiary_civil_status), ['id' => "beneficiary_civil_status", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('beneficiary_civil_status'))
						<span class="help-block">{{$errors->first('beneficiary_civil_status')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_religion') ? 'has-error' : NULL}}">
					<label for="beneficiary_religion" class="control-label col-lg-2 text-right">Beneficiary Religion </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_religion" id="beneficiary_religion" placeholder="" value="{{old('beneficiary_religion', $request->beneficiary_religion)}}">
						@if($errors->first('beneficiary_religion'))
						<span class="help-block">{{$errors->first('beneficiary_religion')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_address') ? 'has-error' : NULL}}">
					<label for="beneficiary_address" class="control-label col-lg-2 text-right">Beneficiary Address </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_address" id="beneficiary_address" placeholder="" value="{{old('beneficiary_address', $request->beneficiary_address)}}">
						@if($errors->first('beneficiary_address'))
						<span class="help-block">{{$errors->first('beneficiary_address')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_education') ? 'has-error' : NULL}}">
					<label for="beneficiary_education" class="control-label col-lg-2 text-right">Beneficiary Education </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_education" id="beneficiary_education" placeholder="" value="{{old('beneficiary_education', $request->beneficiary_education)}}">
						@if($errors->first('beneficiary_education'))
						<span class="help-block">{{$errors->first('beneficiary_education')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_occupation') ? 'has-error' : NULL}}">
					<label for="beneficiary_occupation" class="control-label col-lg-2 text-right">Beneficiary Occupation </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_occupation" id="beneficiary_occupation" placeholder="" value="{{old('beneficiary_occupation', $request->beneficiary_occupation)}}">
						@if($errors->first('beneficiary_occupation'))
						<span class="help-block">{{$errors->first('beneficiary_occupation')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_income') ? 'has-error' : NULL}}">
					<label for="beneficiary_income" class="control-label col-lg-2 text-right">Beneficiary Income </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_income" id="beneficiary_income" placeholder="" value="{{old('beneficiary_income', $request->beneficiary_income)}}">
						@if($errors->first('beneficiary_income'))
						<span class="help-block">{{$errors->first('beneficiary_income')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('beneficiary_rel_to_client') ? 'has-error' : NULL}}">
					<label for="beneficiary_rel_to_client" class="control-label col-lg-2 text-right">Beneficiary Relationship To Client </label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="beneficiary_rel_to_client" id="beneficiary_rel_to_client" placeholder="" value="{{old('beneficiary_rel_to_client', $request->beneficiary_rel_to_client)}}">
						@if($errors->first('beneficiary_rel_to_client'))
						<span class="help-block">{{$errors->first('beneficiary_rel_to_client')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('appointment_schedule') ? 'has-error' : NULL}}">
					<label for="appointment_schedule" class="control-label col-lg-2 text-right">Appointment Schedule</label>
					<div class="col-lg-9">
						<input type="text" id="appointment_schedule" name="appointment_schedule" class="dropup form-control daterange-single" placeholder="YYYY-MM-DD HH:mm" value="{{old('appointment_schedule', $request->tracker->appointment_schedule)}}">
						<span class="help-block">You may ignore this field if this MAC request is not created from the mobile app.</span>
						@if($errors->first('appointment_schedule'))
						<span class="help-block">{{$errors->first('appointment_schedule')}}</span>
						@endif
					</div>
				</div>


				
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.directories.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('#appointment_schedule').daterangepicker({ 
	    	autoApply: true,
			singleDatePicker: true,
			timePicker: true,
			timePicker24Hour: true,
			timePickerIncrement: 60,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});

		$('#beneficiary_birthdate').daterangepicker({ 
			autoApply: true,
			autoUpdateInput: false,
			singleDatePicker: true,
			timePicker: false,
			locale: {
				format: 'YYYY-MM-DD'
			}
		}).on('apply.daterangepicker', function (ev, picker){
			$(this).val(picker.startDate.format("YYYY-MM-DD"));
		});

		$('.select').each(function(){
			$id = "#" + $(this).attr('id') + " option:first";
			$($id).prop('disabled',1);
		});

		$('.select-no-search').select2({
			minimumResultsForSearch: Infinity
		});

		$('.select-with-search').select2();

	});
</script>
@stop